import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyABkVb_zI8649SeC3KFtnxEYZuLttGj2BQ",
  authDomain: "carnegieartcenterdata.firebaseapp.com",
  projectId: "carnegieartcenterdata",
  storageBucket: "carnegieartcenterdata.appspot.com",
  messagingSenderId: "232655009337",
  appId: "1:232655009337:web:10472cab7cd255479cbfe8",
  measurementId: "G-EG2Q6ZE8ZV"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();

export { auth, db };
