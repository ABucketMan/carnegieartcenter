import logo from './logo.svg';
import './App.css';
// import db from './firebase.js';
import { Link } from 'react-router-dom';
import Root from './components/Root';

// components
import Login from './components/Login';

function App() {
  return (
    <div className="App">
      <Root />
      <Link to="/admin"></Link>
    </div>
  );
}

export default App;
