import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import { BrowserRouter, Routes, Route } from 'react-router-dom';
// pages
import Admin from './pages/Admin'
// components
import CreateMember from './components/CreateMember';
import MembershipData from './components/MembershipData';
import SubmitEvent from './components/SubmitEvent';

ReactDOM.render(
  <BrowserRouter>
    <Routes>
      <Route exact path="/" element={<App />} />
      <Route path="/admin" element={<Admin />} />
      <Route path="/createMember" element={<CreateMember />} />
      <Route path="/membershipData" element={<MembershipData />} />
      <Route path="/submitEvent" element={<SubmitEvent />} />
    </Routes>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
