import { initializeApp } from "firebase/app";
import { getAuth, signInWithEmailAndPassword, signOut } from "firebase/auth";
import { credentials } from "../credentials/Credential";

export class Authorize {
  constructor(callback) {
    this.updateOnChange = callback;
  }

  firebaseConfig = credentials;

  app = initializeApp(this.firebaseConfig);
  auth = getAuth();

  signInUser(email, password) {
    signInWithEmailAndPassword(this.auth, email, password)
      .then((userCredential) => {
        // Signed in
        const user = userCredential.user;
        console.log("Signing in!");
        console.log(user);
        // ...
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log(errorCode + " - " + errorMessage);
      });
  }

  logOut() {
    const auth = getAuth();
    signOut(auth)
      .then(() => {
        console.log("Logging out");
        console.log(auth);
      })
      .catch((error) => {
        console.log("An error occured");
        console.log(error);
      });
  }

  getStatus() {
      console.log(this.auth.currentUser);
  }
}
