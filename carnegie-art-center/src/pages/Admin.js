import React from "react";
import './Admin.css';
import { useNavigate } from 'react-router-dom';

// components
import MembershipData from "../components/MembershipData";
import CreateMember from "../components/CreateMember";
import SubmitEvent from "../components/SubmitEvent";

const Admin = (props) => {
  const { logOff } = props;

  const handleLogOut = () => {
    try {
      logOff();
    } catch(err) {
      console.log("Failed to log out");
      console.log(err);
    }
  }

  return (
    <div className="admin-page">
      <h1>Admin</h1>
      <button onClick={handleLogOut}>Log Out</button>
      <p>Welcome back!</p>
      <div>
        <MembershipData />
      </div>
    </div>
  );
};

export default Admin;
