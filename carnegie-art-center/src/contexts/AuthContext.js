import React, { useContext } from "react";
import { auth } from "../firebase";

// Context provides a way to pass data through the component
// tree without having to pass props down manually at every level
// In a typical React application, data is passed top-down
// (parent to child) via props
const AuthContext = React.createContext();

export function useAuth() {
  return useContext(AuthContext);
}

export function AuthProvider({ children }) {
  function login(email, password) {
    auth.signInWithEmailAndPassword(email, password);
  }

  const value = {
    login,
  };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}
