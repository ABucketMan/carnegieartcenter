import React from "react";
import { useNavigate } from "react-router-dom";
import './SubmitEvent.css';

const SubmitEvent = () => {
  //navigate
  const navigate = useNavigate();
  const navigateHandler = () => {
    navigate("/admin")
  }

  return (
    <div className="se-main-section">
      <h2>Submit Event</h2>
      <button onClick={() => { navigateHandler(); }}>+++++++</button>
      <div className="se-section">

      </div>
    </div>
  );
};

export default SubmitEvent;
