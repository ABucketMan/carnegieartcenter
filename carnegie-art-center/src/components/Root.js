import React, { useState } from "react";
import { Authorize } from "../auth/Auth";

// pages
import Login from "./Login";
import Admin from "../pages/Admin";

const Root = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [user, setUser] = useState();

  let auth = new Authorize((authorizedUser, status) => {
    if (authorizedUser) {
      console.log(authorizedUser);
      setUser(authorizedUser);
    } else {
      setUser(undefined);
    }
    setIsLoggedIn(status);
  });

  if (isLoggedIn) {

    return <Admin logOff={() => {
        setIsLoggedIn(false); 
        auth.logOut();
    }} />;
  }
  return <Login signIn={(email, password) => {
      console.log(email, password);
      setIsLoggedIn(true);
      auth.signInUser(email, password);
  }}/>;
};

export default Root;
