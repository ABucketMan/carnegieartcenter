import React, { useState } from "react";
import { auth, db } from '../firebase.js';
import { useNavigate } from "react-router-dom";
import "./CreateMember.css";
// "none", "Individual", "Family", "Friend", "Benefactor", "Patron", "Sustainer"

const CreateMember = () => {
  const [name, setName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [email, setEmail] = useState("");
  const [memberType, setMemberType] = useState("");
  //navigate
  const navigate = useNavigate();
  const navigateHandler = () => {
    navigate("/admin")
  }

  const memberTypes = [
                        { label: 'None', value: 'none' },
                        { label: 'Individual', value: 'Individual' },
                        { label: 'Family', value: 'Family' },
                        { label: 'Friend', value: 'Friend' },
                        { label: 'Benefactor', value: 'Benefactor' },
                        { label: 'Patron', value: 'Patron' },
                        { label: 'Sustainer', value: 'Sustainer' },
                      ];
  const handleChange = (event) => {
    setMemberType(event.target.value)
  };
  //add data
  const addMember = (e) => {
    e.preventDefault();
    db.collection("members").add({
      name: name,
      phoneNumber: phoneNumber,
      email: email,
      memberType: memberType,
    });

    setName("");
    setPhoneNumber("");
    setEmail("");
    setMemberType("");
  };

  return (
    <div className="cm-main-section">
      <h2>Create Member</h2>
      <div className="cm-section">
        <input type="text" placeholder="Enter Name" value={name}
          onChange={(e) => setName(e.target.value)}></input>
        <input type="tel" placeholder="Enter Phone Number" value={phoneNumber}
          onChange={(e) => setPhoneNumber(e.target.value)}></input>
        <input type="email" placeholder="Enter Email" value={email}
          onChange={(e) => setEmail(e.target.value)}></input>
        <div>
          <label>
            Member Types
            <select value={memberType} onChange={handleChange}>
              {memberTypes.map((types) => (
                <option key={types.value} value={types.value}>{types.label}</option>
              ))}
            </select>
          </label>
        </div>
        <button onClick={() => { addMember()}}>Add +</button>
      </div>
    </div>
  );
};

export default CreateMember;
