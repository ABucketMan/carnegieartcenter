import React, { useRef, useState } from "react";
// import { useAuth } from "../contexts/AuthContext";
import { useNavigate } from "react-router-dom";
import { Authorize } from "../auth/Auth";

import './Login.css';

const Login = (props) => {
  const { signIn } = props;

  // useRef is used to store a mutable value that does not cause a re-render when updated.
  const emailRef = useRef();
  const passwordRef = useRef();
  const [error, setError] = useState("");

  function handleSubmit() {
    try {
      setError("");
      signIn(emailRef.current.value, passwordRef.current.value);
      // navigateHandler();
    } catch (err) { 
      setError("Failed to sign in");
      console.log(err);
      console.log(emailRef.current.value, passwordRef.current.value);
    }
  }

  return (
    <div>
      <h1>Sign In for Admin</h1>
      
      <div className="sign-in-section">
        <div></div>
        <div className="sign-in">
          <h3>{error}</h3>
          <h2>Email</h2>
          <input type="email" ref={emailRef} placeholder="Enter Email"></input>
          <h2>Password</h2>
          <input type="password" ref={passwordRef} placeholder="Enter Password"></input>
          <button onClick={handleSubmit}>Sign In</button>
        </div>
        <div></div>
      </div>
    </div>
  );
};

export default Login;
